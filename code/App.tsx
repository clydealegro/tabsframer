import * as React from "react"
import { Override, Data } from "framer"

// Learn more: https://framer.com/docs/overrides/

const appState = Data({
    selected: "Study Abroad",
})

export function Tabs(props): Override {
    return {
        selected: appState.selected,
        onTabClick: directory => {
            appState.selected = directory
        },
    }
}
