import * as React from "react"
import { Frame, Stack, addPropertyControls, ControlType } from "framer"

// Open Preview: Command + P
// Learn more: https://framer.com/api

export function Tabs(props) {
    const { directories, onTabClick, selected } = props

    const handleClick = directory => {
        console.log(directory)
        onTabClick(directory)
    }

    const defaultTabStyle = {
        color: "#444",
        fontFamily: "Maison Neue, sans-serif",
        fontSize: "16px",
    }

    const selectedTabStyle = {
        color: "red",
        fontFamily: "Maison Neue, sans-serif",
        fontSize: "16px",
    }

    return (
        <Stack
            width={"100%"}
            background={"#fff"}
            height={50}
            direction="horizontal"
        >
            {directories.map((directory, index) => {
                return (
                    <Frame
                        height={"100%"}
                        background={"none"}
                        key={index}
                        onTap={() => handleClick(directory)}
                        style={
                            selected === directory
                                ? selectedTabStyle
                                : defaultTabStyle
                        }
                    >
                        {directory}
                    </Frame>
                )
            })}
        </Stack>
    )
}

Tabs.defaultProps = {
    directories: [
        "Study Abroad",
        "Volunteer Abroad",
        "Intern Abroad",
        "Teach Abroad",
        "TEFL",
    ],
    selected: "Study Abroad",
    onTabClick: () => null,
}

// Learn more: https://framer.com/api/property-controls/
